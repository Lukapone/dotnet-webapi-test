// ASP.NET Core middleware

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Http.Headers;
using Microsoft.Net.Http.Headers;
using System;
using System.Threading.Tasks;

namespace MyApp.Middleware
{
    public class MyMiddleware
    {
        private readonly RequestDelegate _next;

        public MyMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            // Do something with context near the beginning of request processing.
            Console.WriteLine("In the Invoke.");
            Console.WriteLine(httpContext.Items.Keys.Count);
            Console.WriteLine(httpContext.TraceIdentifier);
            Console.WriteLine(httpContext.Request.Method);
            Console.WriteLine(httpContext.Request.GetDisplayUrl());
            Console.WriteLine(httpContext.Request.IsHttps);
            Console.WriteLine(httpContext.Connection.RemoteIpAddress?.ToString());
            Console.WriteLine(httpContext.Request.Headers.Values.Count);
            Console.WriteLine(httpContext.Request.Headers[HeaderNames.UserAgent].ToString());
            Console.WriteLine();
            Console.WriteLine();

            await _next.Invoke(httpContext);

            // Clean up.
        }
    }

    public static class MyMiddlewareExtensions
    {
        public static IApplicationBuilder UseMyMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<MyMiddleware>();
        }
    }
}