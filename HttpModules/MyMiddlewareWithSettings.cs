


using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

public class MyMiddlewareWithSettings
{
    private readonly RequestDelegate _next;
    private readonly CustomMiddlewareSettings _myMiddlewareOptions;

    public MyMiddlewareWithSettings(RequestDelegate next,
        IOptions<CustomMiddlewareSettings> optionsAccessor)
    {
        _next = next;
        _myMiddlewareOptions = optionsAccessor.Value;
    }

    public async Task Invoke(HttpContext context)
    {
        // Do something with context near the beginning of request processing
        // using configuration in _myMiddlewareOptions
        System.Console.WriteLine($"Parameterr { _myMiddlewareOptions.Param1}");
        await _next.Invoke(context);

        // Do something with context near the end of request processing
        // using configuration in _myMiddlewareOptions
    }


}


public static class MyMiddlewareWithParamsExtensions
{
    public static IApplicationBuilder UseMyMiddlewareWithParams(
        this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<MyMiddlewareWithSettings>();
    }

    public static IApplicationBuilder UseMyMiddlewareWithParams(
        this IApplicationBuilder builder, CustomMiddlewareSettings myMiddlewareOptions)
    {
        return builder.UseMiddleware<MyMiddlewareWithSettings>(
            new OptionsWrapper<CustomMiddlewareSettings>(myMiddlewareOptions));
    }
}