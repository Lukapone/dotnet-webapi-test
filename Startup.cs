﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TodoApi.Models;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;
using MyApp.Middleware;

namespace TodoApi
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                        .SetBasePath(env.ContentRootPath)
                        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                        .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TodoContext>(opt => opt.UseInMemoryDatabase("TodoList"));
            services.AddMvc();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "My ToDo API",
                    Version = "v1",
                    Description = "A simple example ASP.NET Core Web API",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "Lukas Ponik",
                        Email = "lukapone2@gmail.com",
                        Url = "the url"
                    },
                    License = new License
                    {
                        Name= "User Licence",
                        Url =" example url"
                    }
                });
                // Set the comments path for the Swagger JSON and UI.
                var basePath = AppContext.BaseDirectory;
                var xmlPath = Path.Combine(basePath, "TodoApi.xml");
                c.IncludeXmlComments(xmlPath);
            });

            // Setup options service
            services.AddOptions();
            //// Load options from section "MyMiddlewareOptionsSection"
            //services.Configure<CustomMiddlewareSettings>(
            //    Configuration.GetSection("MyMiddlewareOptionsSection"));


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }

            app.UseMyMiddleware();
            var myMiddlewareOptions = Configuration.GetSection("MyMiddlewareOptionsSection").Get<CustomMiddlewareSettings>();
            var myMiddlewareOptions2 = Configuration.GetSection("MyMiddlewareOptionsSection2").Get<CustomMiddlewareSettings>();
            app.UseMyMiddlewareWithParams(myMiddlewareOptions);
            app.UseMyMiddlewareWithParams(myMiddlewareOptions2);
            // Create branch to the MyHandlerMiddleware. 
            // All requests ending in .report will follow this branch.
            app.MapWhen(
                context => context.Request.Path.ToString().EndsWith(".report"),
                appBranch =>
                {
                    // ... optionally add more middleware to this branch
                    appBranch.UseMyHandler();
                });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
            app.UseMvc();

            //var serverAddressesFeature = app.ServerFeatures.Get<IServerAddressesFeature>();

            //app.Run(async (context) =>
            //{
            //    context.Response.ContentType = "text/html";
            //    await context.Response
            //        .WriteAsync("<p>Hosted by Kestrel</p>");

            //    if (serverAddressesFeature != null)
            //    {
            //        await context.Response
            //            .WriteAsync("<p>Listening on the following addresses: " +
            //                string.Join(", ", serverAddressesFeature.Addresses) +
            //                "</p>");
            //    }

            //    await context.Response.WriteAsync($"<p>Request URL: {context.Request.GetDisplayUrl()}<p>");
            //});

        }

    }
    
}
